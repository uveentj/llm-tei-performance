# LLM TEI performance tests

how well do different ollama models solve TEI related questions

you need to have [ollama](https://ollama.ai/) running somewhere (default to localhost)

```bash
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt

./runtests.py
```

you can use another host than localhost as ollama server
```bash
OLLAMA_HOST=http://nbox:11434 ./runtests.py
```

questions, models and output directory are configured in [tests.yml](tests.yml).
you need to download the models with `ollama pull <modelname>` first.

pulling a model can also be done by code, check the used 
[ollama python library](https://github.com/ollama/ollama-python/)
for details how to implement.