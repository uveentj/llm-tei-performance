#!/usr/bin/env python

# based on https://github.com/ollama/ollama/blob/main/examples/python-simplegenerate/client.py
# todo: is chat api better? https://github.com/ollama/ollama/tree/main/examples/python-simplechat
# https://ollama.ai/blog/python-javascript-libraries

import yaml
import ollama
import os

from get_code_from_markdown import *

# NOTE: ollama must be running for this to work, start the ollama app or run `ollama serve`

def main():

    # os.getenv('OLLAMA_HOST') is used 
    #client = ollama.Client(host='http://nbox:11434')

    # load tests config
    with open('./tests.yml', 'r', encoding='utf-8') as stream:
        config = yaml.safe_load(stream)

    print("|✨ config: ", config)

    for model in config['models']:

        print('|✨ model:', model)
        outdir = config['output']+'/'+model
        os.makedirs(outdir, exist_ok=True)

        info = ollama.show(model)
        with open(outdir+'/'+model+'.Modelfile', 'w') as file:
            file.write(info['modelfile'])        

        context = [] # the context stores a conversation history, you can use this to make the model more context aware
        for cnt, question in enumerate(config['questions']):
            print('|✨ question:', question)
            response = ollama.generate(model=model, prompt=question, context=context)
            context = response['context']
            print('💬', response['response'])
            with open(outdir+'/question'+str(cnt)+'.md', 'w') as file:
                file.write(response['response'])

            xml = get_code_from_markdown(response['response'], language='xml')
            print('|✨ found xml:', xml)
            if len(xml) > 0:
                with open(outdir+'/question'+str(cnt)+'-teisample.xml', 'w') as file:
                    file.write(xml[0])           

        with open(outdir+'/context.txt', 'w') as file:
            file.write(str(context))  

if __name__ == "__main__":
    main()
