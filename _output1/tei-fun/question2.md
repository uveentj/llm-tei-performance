Sicher! Hier ist ein einfaches TEI Dokument, das einen literarischen Text enthält:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Ein einfaches TEI Beispiel</title>
      </titleStmt>
      <publicationStmt>
        <p>Dieses Dokument wurde erstellt am 01.01.2022.</p>
      </publicationStmt>
    </fileDesc>
  </teiHeader>
  <text>
    <body>
      <div type="chapter">
        <head>Einführung in das TEI Encoding</head>
        <p>TEI, die Text Encoding Initiative, ist eine Sammlung von XML-basierten Standards und Richtlinien zur Kodierung von Textdokumenten. Die Spezifikation bietet eine Vielzahl von Elementen und Attributen, die speziell für verschiedene Textarten entwickelt wurden.</p>
        <p class="note">Dieses Dokument ist natürlich nur ein Beispiel und kann noch weiter ausgebaut werden.</p>
      </div>
    </body>
  </text>
</TEI>
```