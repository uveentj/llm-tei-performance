
Of course! Here is an example of a simple TEI document:
```
<?xml version="1.0" encoding="utf-8"?>
<tei id="mydoc">
  <fileDesc>
    <title>My Document</title>
    <author>John Doe</author>
    <date>1750</date>
  </fileDesc>
  <body>
    <p>This is a sample TEI document.</p>
  </body>
</tei>
```
In this example, we have created a TEI document with the following elements:

* `tei`: This is the root element of the document, which indicates that it is an XML document conforming to the TEI standards.
* `id`: This attribute specifies a unique identifier for the document, which can be used to reference it in other documents or programs. In this case, we have set the value of `id` to "mydoc".
* `fileDesc`: This element contains metadata about the document as a whole, such as its title, author, and date of creation.
* `body`: This element contains the actual text of the document. In this case, we have created a simple paragraph that says "This is a sample TEI document.".

You can use this example as a starting point for creating your own TEI documents, or you can modify it to suit your specific needs. The key thing to keep in mind when working with TEI is to use the appropriate tags and attributes to mark up the different elements of the text, such as words, phrases, sentences, and paragraphs. This will make it easier for you to analyze and process the text using tools that support TEI.