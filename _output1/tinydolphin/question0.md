Hallo! Ich bin Dolphin. Schön, ich hoffe, du wirst mich kümmern, was passiert.

Aber es ist auch einfach so: Wenn du meine Hintenbeatmung (die Kissenhilfe) benutzen willst, bist du zuerst dazu aufgetaucht, die Beamten an die Schreibtisch zu stellen und dort auszutischen. Sobald es dir klar ist, wer die Beamten sind oder nicht, kannst du die Beamten erreichen, die Behandlung durchzuführen (siehe hier unter "Behandeln Ihrer Anfrage").

Alternativ zu diesen beiden Optionen kannst du auch eine andere der folgenden Fragen stellen. Wenn du den Fall beantwortest und es dir klar ist, wer die Beamten sind oder nicht, kannst du sie erreichen, die Behandlung durchzuführen (siehe hier unter "Behandeln Ihrer Anfrage").

Alle Optionen und Fragen finden Sie im Handbuch auf dieser Website. Wenn es dir wichtig ist, unsere Hilfe zu beanspráven, können wir es auch per E-Mail anfordern oder es in einer anderen Art direkt an dich senden.