Hier ist ein einfaches TEI-Beispieldokument:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Beispieltitel</title>
      </titleStmt>
      <editionStmt>
        <respStmt>
          <name>Dolphin</name>
        </respStmt>
      </editionStmt>
    </fileDesc>
  </teiHeader>
  <text>
    <body>
      <p>Hier ist ein Absatz mit Text.</p>
    </body>
  </text>
</TEI>
```
In diesem Beispiel haben wir einen TEI-Kopf (`<teiHeader>`) und eine Haupttextstruktur (`<text>`). Der Kopf enthält Informationen über den Dateinamen, den Titel des Dokuments und die Edition. Der Hauptteil beinhaltet einen Absatz mit Text (`<p>Hier ist ein Absatz mit Text.</p>`).