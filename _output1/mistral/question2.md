 Ja, von course! I'll provide you with a simple example of a TEI document that encodes a short text from the works of William Shakespeare - the famous monologue "To be or not to be" from Hamlet. This example will cover the basics of structuring an XML document using TEI, encoding text and adding some basic metadata.

First, let's create a new file named `hamlet_monologue.xml`. Then paste the following code into your editor:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title type="main">To be or not to be</title>
        <author>William Shakespeare</author>
      </titleStmt>
      <publicationStmt>
        <publisher>My Digital Library</publisher>
        <date type="production">2023-01-01</date>
      </publicationStmt>
    </fileDesc>
  </teiHeader>
  <text>
    <head>
      <title type="work">Act III, Scene i</title>
      <title type="document">Hamlet: To be or not to be monologue</title>
    </head>
    <body>
      <p>
        <speaker xml:id="hamlet">
          <personName>
            <forename type="given">Hamlet</forename>
            <surname type="family">of Denmark</surname>
          </personName>
          <quoted speechRole="spkr">To be, or not to be: that is the question:</quoted>
        </speaker>
      </p>
      <p>
        <speaker xml:id="hamlet">
          <quoted speechRole="spkr">Whether 'tis nobler in the mind to suffer</quoted>
          <quoted speechRole="spkr">The slings and arrows of outrageous fortune,</quoted>
          <quoted speechRole="spkr">Or to take arms against a sea of troubles,</quoted>
          <quoted speechRole="spkr">And by opposing end them? To die: to sleep;</quoted>
          <quoted speechRole="spkr">No more; and by a sleep to say we end</quoted>
          <quoted speechRole="spkr">The heartache and the thousand natural shocks</quoted>
          <quoted speechRole="spkr">That flesh is heir to? 'Tis a consummation</quoted>
          <quoted speechRole="spkr">Devoutly to be wished. To die, to sleep;</quoted>
          <quoted speechRole="spkr">To sleep: perchance to dream: ay, there's the rub;</quoted>
          <quoted speechRole="spkr">For in that sleep of death what dreams may come</quoted>
          <quoted speechRole="spkr">When we have shuffled off this mortal coil,</quoted>
          <quoted speechRole="spkr">Must give us pause: there's the respect</quoted>
          <quoted speechRole="spkr">That makes calamity of so long life:</quoted>
          <quoted speechRole="spkr">For who would bear the whips and scorns of time,</quoted>
          <quoted speechRole="spkr">Th' oppressor's wrong, the proud man's contumely,</quoted>
          <quoted speechRole="spkr">The pangs of despised love, the law's delay,</quoted>
          <quoted speechRole="spkr">The insolence of office and the spurns</quoted>
          <quoted speechRole="spkr">That patient merit deserves? 'Gainst death itself:</quoted>
          <quoted speechRole="spkr">Cows fear a falling tree;</quoted>
          <quoted speechRole="spkr">I'll have no more of it:</quoted>
        </speaker>
      </p>
    </body>
  </text>
</TEI>
```

This example consists of several parts:

1. The first lines declare the XML version and the TEI namespace.
2. `<teiHeader>` contains metadata about the document and file.
3. `<fileDesc>` includes information about the title, author, and publisher.
4. `<publicationStmt>` describes when the text was published.
5. The actual content is encapsulated within `<text>`.
6. Inside `<text>`, we have a header with the title of the work and document.
7. The body contains the spoken text, structured by `<p>` tags and attributed to Hamlet using `<speaker>`.

This example should give you a good starting point for creating TEI documents. You can customize and extend it according to your needs! If you have any questions or need further clarification, don't hesitate to ask.